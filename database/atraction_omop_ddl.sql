-- DROP SEQUENCE public.epigen_exp_epigen_exp_id_seq;

CREATE SEQUENCE public.epigen_exp_epigen_exp_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START 1
	CACHE 1
	NO CYCLE;
	
-- DROP SEQUENCE public.epigen_modification_epigen_modification_id_seq;

CREATE SEQUENCE public.epigen_modification_epigen_modification_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START 1
	CACHE 1
	NO CYCLE;
	
-- DROP SEQUENCE public.metag_result_metag_result_id_seq;

CREATE SEQUENCE public.metag_result_metag_result_id_seq
	INCREMENT BY 1
	MINVALUE 1
	MAXVALUE 2147483647
	START 1
	CACHE 1
	NO CYCLE;


-- public."location" definition

-- Drop table

-- DROP TABLE public."location";

CREATE TABLE public."location" (
	location_id int8 NOT NULL,
	address_1 varchar(50) NULL,
	address_2 varchar(50) NULL,
	city varchar(50) NULL,
	state varchar(2) NULL,
	zip varchar(9) NULL,
	county varchar(20) NULL,
	country varchar(100) NULL,
	location_source_value varchar(50) NULL,
	latitude numeric NULL,
	longitude numeric NULL,
	CONSTRAINT xpk_location PRIMARY KEY (location_id)
);


-- public.bacterial_gene definition

-- Drop table

-- DROP TABLE public.bacterial_gene;

CREATE TABLE public.bacterial_gene (
	bacterial_gene_id int4 NOT NULL,
	concept_id int4 NULL,
	species_id int4 NULL,
	"function" int4 NULL,
	CONSTRAINT bacterial_gene_pkey PRIMARY KEY (bacterial_gene_id)
);


-- public.care_site definition

-- Drop table

-- DROP TABLE public.care_site;

CREATE TABLE public.care_site (
	care_site_id int8 NOT NULL,
	care_site_name varchar(255) NULL,
	place_of_service_concept_id int4 NOT NULL,
	location_id int8 NULL,
	care_site_source_value varchar(50) NULL,
	place_of_service_source_value varchar(50) NULL,
	CONSTRAINT xpk_care_site PRIMARY KEY (care_site_id)
);


-- public.concept definition

-- Drop table

-- DROP TABLE public.concept;

CREATE TABLE public.concept (
	concept_id int4 NOT NULL,
	concept_name varchar(255) NOT NULL,
	domain_id varchar(20) NOT NULL,
	vocabulary_id varchar(20) NOT NULL,
	concept_class_id varchar(20) NOT NULL,
	standard_concept varchar(1) NULL,
	concept_code varchar(50) NOT NULL,
	valid_start_date date NOT NULL,
	valid_end_date date NOT NULL,
	invalid_reason varchar(1) NULL,
	CONSTRAINT xpk_concept PRIMARY KEY (concept_id)
);
CREATE INDEX idx_concept_class_id ON public.concept USING btree (concept_class_id);
CREATE INDEX idx_concept_code ON public.concept USING btree (concept_code);
CREATE UNIQUE INDEX idx_concept_concept_id ON public.concept USING btree (concept_id);
CREATE INDEX idx_concept_domain_id ON public.concept USING btree (domain_id);
CREATE INDEX idx_concept_id_varchar ON public.concept USING btree (((concept_id)::character varying));
CREATE INDEX idx_concept_vocabluary_id ON public.concept USING btree (vocabulary_id);


-- public.concept_class definition

-- Drop table

-- DROP TABLE public.concept_class;

CREATE TABLE public.concept_class (
	concept_class_id varchar(20) NOT NULL,
	concept_class_name varchar(255) NOT NULL,
	concept_class_concept_id int4 NOT NULL,
	CONSTRAINT xpk_concept_class PRIMARY KEY (concept_class_id)
);
CREATE UNIQUE INDEX idx_concept_class_class_id ON public.concept_class USING btree (concept_class_id);


-- public.condition_occurrence definition

-- Drop table

-- DROP TABLE public.condition_occurrence;

CREATE TABLE public.condition_occurrence (
	condition_occurrence_id int8 NOT NULL,
	person_id int8 NOT NULL,
	condition_concept_id int4 NOT NULL,
	condition_start_date date NULL,
	condition_start_datetime timestamp NOT NULL,
	condition_end_date date NULL,
	condition_end_datetime timestamp NULL,
	condition_type_concept_id int4 NOT NULL,
	condition_status_concept_id int4 NOT NULL,
	stop_reason varchar(20) NULL,
	provider_id int8 NULL,
	visit_occurrence_id int8 NULL,
	visit_detail_id int8 NULL,
	condition_source_value varchar(50) NULL,
	condition_source_concept_id int4 NOT NULL,
	condition_status_source_value varchar(50) NULL,
	CONSTRAINT xpk_condition_occurrence PRIMARY KEY (condition_occurrence_id)
);
CREATE INDEX idx_condition_concept_id ON public.condition_occurrence USING btree (condition_concept_id);
CREATE INDEX idx_condition_person_id ON public.condition_occurrence USING btree (person_id);
CREATE INDEX idx_condition_visit_id ON public.condition_occurrence USING btree (visit_occurrence_id);


-- public."domain" definition

-- Drop table

-- DROP TABLE public."domain";

CREATE TABLE public."domain" (
	domain_id varchar(20) NOT NULL,
	domain_name varchar(255) NOT NULL,
	domain_concept_id int4 NOT NULL,
	CONSTRAINT xpk_domain PRIMARY KEY (domain_id)
);
CREATE UNIQUE INDEX idx_domain_domain_id ON public.domain USING btree (domain_id);


-- public.drug_exposure definition

-- Drop table

-- DROP TABLE public.drug_exposure;

CREATE TABLE public.drug_exposure (
	drug_exposure_id int8 NOT NULL,
	person_id int8 NOT NULL,
	drug_concept_id int4 NOT NULL,
	drug_exposure_start_date date NULL,
	drug_exposure_start_datetime timestamp NOT NULL,
	drug_exposure_end_date date NULL,
	drug_exposure_end_datetime timestamp NOT NULL,
	verbatim_end_date date NULL,
	drug_type_concept_id int4 NOT NULL,
	stop_reason varchar(20) NULL,
	refills int4 NULL,
	quantity numeric NULL,
	days_supply int4 NULL,
	sig text NULL,
	route_concept_id int4 NOT NULL,
	lot_number varchar(50) NULL,
	provider_id int8 NULL,
	visit_occurrence_id int8 NULL,
	visit_detail_id int8 NULL,
	drug_source_value varchar(50) NULL,
	drug_source_concept_id int4 NOT NULL,
	route_source_value varchar(50) NULL,
	dose_unit_source_value varchar(50) NULL,
	CONSTRAINT xpk_drug_exposure PRIMARY KEY (drug_exposure_id)
);
CREATE INDEX idx_drug_concept_id ON public.drug_exposure USING btree (drug_concept_id);
CREATE INDEX idx_drug_person_id ON public.drug_exposure USING btree (person_id);
CREATE INDEX idx_drug_visit_id ON public.drug_exposure USING btree (visit_occurrence_id);


-- public.epigen_exp definition

-- Drop table

-- DROP TABLE public.epigen_exp;

CREATE TABLE public.epigen_exp (
	epigen_exp_id serial NOT NULL,
	omic_experiment_id int4 NULL,
	CONSTRAINT epigen_exp_pk PRIMARY KEY (epigen_exp_id)
);


-- public.epigen_modification definition

-- Drop table

-- DROP TABLE public.epigen_modification;

CREATE TABLE public.epigen_modification (
	epigen_modification_id serial NOT NULL,
	epigen_exp_id int4 NULL,
	gene_id int4 NOT NULL,
	CONSTRAINT epigen_modification_pk PRIMARY KEY (epigen_modification_id)
);


-- public.epigen_result definition

-- Drop table

-- DROP TABLE public.epigen_result;

CREATE TABLE public.epigen_result (
	epigen_results_id int4 NOT NULL,
	epigen_experiment_id int4 NULL,
	omic_experiment_id int4 NULL,
	epigen_modification_id int4 NULL,
	CONSTRAINT epigen_results_pk PRIMARY KEY (epigen_results_id)
);


-- public.gene definition

-- Drop table

-- DROP TABLE public.gene;

CREATE TABLE public.gene (
	gene_id int4 NOT NULL,
	"name" varchar NOT NULL,
	concept_id int8 NULL,
	"location" varchar NULL,
	CONSTRAINT "pk_Gene" PRIMARY KEY (gene_id)
);


-- public.lipid definition

-- Drop table

-- DROP TABLE public.lipid;

CREATE TABLE public.lipid (
	lipid_id int8 NOT NULL,
	concept_id int8 NULL,
	"name" varchar NULL,
	"function" varchar NULL,
	CONSTRAINT lipid_pk PRIMARY KEY (lipid_id)
);


-- public.lipid_exp definition

-- Drop table

-- DROP TABLE public.lipid_exp;

CREATE TABLE public.lipid_exp (
	lipid_exp_id int8 NOT NULL,
	lipid_group_id int8 NULL,
	omic_experiment_id int4 NULL,
	CONSTRAINT mass_spectrometry_experiment_pk PRIMARY KEY (lipid_exp_id)
);


-- public.lipid_result definition

-- Drop table

-- DROP TABLE public.lipid_result;

CREATE TABLE public.lipid_result (
	lipid_result_id int8 NOT NULL,
	lipid_exp_id int8 NULL,
	lipid_id int8 NULL,
	expression_percentile float8 NULL,
	normalized_expression float8 NULL,
	fdr float8 NULL,
	spectral_count int8 NULL,
	unique_peptides int8 NULL,
	seq_coverage float8 NULL,
	omic_experiment_id int4 NOT NULL,
	CONSTRAINT mass_spectrometry_result_pk PRIMARY KEY (lipid_result_id)
);


-- public.location_history definition

-- Drop table

-- DROP TABLE public.location_history;

CREATE TABLE public.location_history (
	location_history_id int8 NOT NULL,
	location_id int8 NOT NULL,
	relationship_type_concept_id int4 NOT NULL,
	domain_id varchar(50) NOT NULL,
	entity_id int8 NOT NULL,
	start_date date NOT NULL,
	end_date date NULL,
	CONSTRAINT xpk_location_history PRIMARY KEY (location_history_id)
);


-- public.measurement definition

-- Drop table

-- DROP TABLE public.measurement;

CREATE TABLE public.measurement (
	measurement_id int8 NOT NULL,
	person_id int8 NOT NULL,
	measurement_concept_id int4 NOT NULL,
	measurement_date date NULL,
	measurement_datetime timestamp NOT NULL,
	measurement_time varchar(10) NULL,
	measurement_type_concept_id int4 NOT NULL,
	operator_concept_id int4 NULL,
	value_as_number numeric NULL,
	value_as_concept_id int4 NULL,
	unit_concept_id int4 NULL,
	range_low numeric NULL,
	range_high numeric NULL,
	provider_id int8 NULL,
	visit_occurrence_id int8 NULL,
	visit_detail_id int8 NULL,
	measurement_source_value varchar(50) NULL,
	measurement_source_concept_id int4 NOT NULL,
	unit_source_value varchar(50) NULL,
	value_source_value varchar(50) NULL,
	CONSTRAINT xpk_measurement PRIMARY KEY (measurement_id)
);
CREATE INDEX idx_measurement_concept_id ON public.measurement USING btree (measurement_concept_id);
CREATE INDEX idx_measurement_person_id ON public.measurement USING btree (person_id);
CREATE INDEX idx_measurement_visit_id ON public.measurement USING btree (visit_occurrence_id);


-- public.metabo_exp definition

-- Drop table

-- DROP TABLE public.metabo_exp;

CREATE TABLE public.metabo_exp (
	metabo_exp_id int8 NOT NULL,
	metabo_group_id int8 NULL,
	omic_experiment_id int4 NULL,
	CONSTRAINT metabolomic_experiment_pk PRIMARY KEY (metabo_exp_id)
);


-- public.metabo_molecule definition

-- Drop table

-- DROP TABLE public.metabo_molecule;

CREATE TABLE public.metabo_molecule (
	metabo_molecule_id int4 NOT NULL,
	concept_id int8 NULL,
	pathway_id int8 NULL,
	molecule_fold_change float8 NULL,
	molecule_p_value_ajusted float8 NULL,
	metabo_exp_id int8 NULL,
	"name" varchar NOT NULL,
	omic_experiment_id int4 NULL,
	CONSTRAINT metabolite_pk PRIMARY KEY (metabo_molecule_id)
);


-- public.metabo_pathway definition

-- Drop table

-- DROP TABLE public.metabo_pathway;

CREATE TABLE public.metabo_pathway (
	metabo_pathway_id int4 NOT NULL,
	"name" varchar NULL,
	sub_pathway_name varchar NULL,
	sub_pathway_concept_id int8 NULL,
	concept_id int8 NULL,
	CONSTRAINT pathway_pk PRIMARY KEY (metabo_pathway_id)
);


-- public.metag_exp definition

-- Drop table

-- DROP TABLE public.metag_exp;

CREATE TABLE public.metag_exp (
	metag_exp_id int4 NOT NULL,
	metag_group_id int4 NULL,
	omic_experiment_id int8 NULL,
	CONSTRAINT metagenomic_experiment_pkey PRIMARY KEY (metag_exp_id)
);


-- public.metag_result definition

-- Drop table

-- DROP TABLE public.metag_result;

CREATE TABLE public.metag_result (
	metag_result_id serial NOT NULL,
	metag_exp_id int4 NOT NULL,
	omic_experiment_id int4 NULL,
	CONSTRAINT metag_result_pk PRIMARY KEY (metag_result_id)
);


-- public.note definition

-- Drop table

-- DROP TABLE public.note;

CREATE TABLE public.note (
	note_id int8 NOT NULL,
	person_id int8 NOT NULL,
	note_event_id int8 NULL,
	note_event_field_concept_id int4 NOT NULL,
	note_date date NULL,
	note_datetime timestamp NOT NULL,
	note_type_concept_id int4 NOT NULL,
	note_class_concept_id int4 NOT NULL,
	note_title varchar(250) NULL,
	note_text text NULL,
	encoding_concept_id int4 NOT NULL,
	language_concept_id int4 NOT NULL,
	provider_id int8 NULL,
	visit_occurrence_id int8 NULL,
	visit_detail_id int8 NULL,
	note_source_value varchar(50) NULL,
	CONSTRAINT xpk_note PRIMARY KEY (note_id)
);
CREATE INDEX idx_note_concept_id ON public.note USING btree (note_type_concept_id);
CREATE INDEX idx_note_person_id ON public.note USING btree (person_id);
CREATE INDEX idx_note_visit_id ON public.note USING btree (visit_occurrence_id);


-- public.observation definition

-- Drop table

-- DROP TABLE public.observation;

CREATE TABLE public.observation (
	observation_id int8 NOT NULL,
	person_id int8 NOT NULL,
	observation_concept_id int4 NOT NULL,
	observation_date date NULL,
	observation_datetime timestamp NOT NULL,
	observation_type_concept_id int4 NOT NULL,
	value_as_number numeric NULL,
	value_as_string varchar(60) NULL,
	value_as_concept_id int4 NULL,
	qualifier_concept_id int4 NULL,
	unit_concept_id int4 NULL,
	provider_id int8 NULL,
	visit_occurrence_id int8 NULL,
	visit_detail_id int8 NULL,
	observation_source_value varchar(50) NULL,
	observation_source_concept_id int4 NOT NULL,
	unit_source_value varchar(50) NULL,
	qualifier_source_value varchar(50) NULL,
	observation_event_id int8 NULL,
	obs_event_field_concept_id int4 NOT NULL,
	value_as_datetime timestamp NULL,
	CONSTRAINT xpk_observation PRIMARY KEY (observation_id)
);
CREATE INDEX idx_observation_concept_id ON public.observation USING btree (observation_concept_id);
CREATE INDEX idx_observation_person_id ON public.observation USING btree (person_id);
CREATE INDEX idx_observation_visit_id ON public.observation USING btree (visit_occurrence_id);


-- public.observation_period definition

-- Drop table

-- DROP TABLE public.observation_period;

CREATE TABLE public.observation_period (
	observation_period_id int8 NOT NULL,
	person_id int8 NOT NULL,
	observation_period_start_date date NOT NULL,
	observation_period_end_date date NOT NULL,
	period_type_concept_id int4 NOT NULL,
	CONSTRAINT xpk_observation_period PRIMARY KEY (observation_period_id)
);
CREATE INDEX idx_observation_period_id ON public.observation_period USING btree (person_id);


-- public.omic_experiment definition

-- Drop table

-- DROP TABLE public.omic_experiment;

CREATE TABLE public.omic_experiment (
	omic_experiment_id int4 NOT NULL,
	experiment_type varchar NULL,
	omic_experiment_date date NOT NULL,
	raw_filepath varchar NULL,
	transform_sample_id int4 NULL,
	group_id int4 NULL,
	CONSTRAINT "pk_Omic_experiment" PRIMARY KEY (omic_experiment_id)
);


-- public.person definition

-- Drop table

-- DROP TABLE public.person;

CREATE TABLE public.person (
	person_id int8 NOT NULL,
	gender_concept_id int4 NOT NULL,
	year_of_birth int4 NOT NULL,
	month_of_birth int4 NULL,
	day_of_birth int4 NULL,
	birth_datetime timestamp NULL,
	death_datetime timestamp NULL,
	race_concept_id int4 NOT NULL,
	ethnicity_concept_id int4 NOT NULL,
	location_id int8 NULL,
	provider_id int8 NULL,
	care_site_id int8 NULL,
	person_source_value varchar(50) NULL,
	gender_source_value varchar(50) NULL,
	gender_source_concept_id int4 NOT NULL,
	race_source_value varchar(50) NULL,
	race_source_concept_id int4 NOT NULL,
	ethnicity_source_value varchar(50) NULL,
	ethnicity_source_concept_id int4 NOT NULL,
	CONSTRAINT xpk_person PRIMARY KEY (person_id)
);
CREATE UNIQUE INDEX idx_person_id ON public.person USING btree (person_id);


-- public.procedure_occurrence definition

-- Drop table

-- DROP TABLE public.procedure_occurrence;

CREATE TABLE public.procedure_occurrence (
	procedure_occurrence_id int8 NOT NULL,
	person_id int8 NOT NULL,
	procedure_concept_id int4 NOT NULL,
	procedure_date date NULL,
	procedure_datetime timestamp NOT NULL,
	procedure_type_concept_id int4 NOT NULL,
	modifier_concept_id int4 NOT NULL,
	quantity int4 NULL,
	provider_id int8 NULL,
	visit_occurrence_id int8 NULL,
	visit_detail_id int8 NULL,
	procedure_source_value varchar(50) NULL,
	procedure_source_concept_id int4 NOT NULL,
	modifier_source_value varchar(50) NULL,
	CONSTRAINT xpk_procedure_occurrence PRIMARY KEY (procedure_occurrence_id)
);
CREATE INDEX idx_procedure_concept_id ON public.procedure_occurrence USING btree (procedure_concept_id);
CREATE INDEX idx_procedure_person_id ON public.procedure_occurrence USING btree (person_id);
CREATE INDEX idx_procedure_visit_id ON public.procedure_occurrence USING btree (visit_occurrence_id);


-- public.protein definition

-- Drop table

-- DROP TABLE public.protein;

CREATE TABLE public.protein (
	protein_id int4 NOT NULL,
	"name" varchar NULL,
	"type" varchar NULL,
	concept_id int4 NULL,
	coding_gene_id int4 NULL,
	protein_exp_id int4 NULL,
	CONSTRAINT protein_pk PRIMARY KEY (protein_id)
);


-- public.proteo_exp definition

-- Drop table

-- DROP TABLE public.proteo_exp;

CREATE TABLE public.proteo_exp (
	proteo_exp_id int4 NOT NULL,
	group_id int4 NULL,
	omic_experiment_id int4 NULL,
	CONSTRAINT protein_experiment_pk PRIMARY KEY (proteo_exp_id)
);


-- public.proteo_result definition

-- Drop table

-- DROP TABLE public.proteo_result;

CREATE TABLE public.proteo_result (
	proteo_result_id int4 NOT NULL,
	proteomic_exp_id int4 NOT NULL,
	protein_id int4 NOT NULL,
	fold_change float4 NULL,
	p_value float4 NULL,
	p_value_adjusted float4 NULL,
	omic_experiment_id int4 NULL,
	CONSTRAINT protein_result_pk PRIMARY KEY (proteo_result_id)
);


-- public.relationship definition

-- Drop table

-- DROP TABLE public.relationship;

CREATE TABLE public.relationship (
	relationship_id varchar(20) NOT NULL,
	relationship_name varchar(255) NOT NULL,
	is_hierarchical varchar(1) NOT NULL,
	defines_ancestry varchar(1) NOT NULL,
	reverse_relationship_id varchar(20) NOT NULL,
	relationship_concept_id int4 NOT NULL,
	CONSTRAINT xpk_relationship PRIMARY KEY (relationship_id)
);
CREATE UNIQUE INDEX idx_relationship_rel_id ON public.relationship USING btree (relationship_id);


-- public.species definition

-- Drop table

-- DROP TABLE public.species;

CREATE TABLE public.species (
	species_id int4 NOT NULL,
	"name" int4 NULL,
	concept_id int4 NULL,
	species_taxonomy int4 NULL,
	abondance_of_species int4 NULL,
	metagenomic_exp_id int8 NOT NULL,
	CONSTRAINT species_pkey PRIMARY KEY (species_id)
);


-- public.specimen definition

-- Drop table

-- DROP TABLE public.specimen;

CREATE TABLE public.specimen (
	specimen_id int8 NOT NULL,
	person_id int8 NOT NULL,
	specimen_concept_id int4 NOT NULL,
	specimen_type_concept_id int4 NOT NULL,
	specimen_date date NULL,
	specimen_datetime timestamp NOT NULL,
	quantity numeric NULL,
	unit_concept_id int4 NULL,
	anatomic_site_concept_id int4 NOT NULL,
	disease_status_concept_id int4 NOT NULL,
	specimen_source_id varchar(50) NULL,
	specimen_source_value varchar(50) NULL,
	unit_source_value varchar(50) NULL,
	anatomic_site_source_value varchar(50) NULL,
	disease_status_source_value varchar(50) NULL,
	CONSTRAINT xpk_specimen PRIMARY KEY (specimen_id)
);
CREATE INDEX idx_specimen_concept_id ON public.specimen USING btree (specimen_concept_id);
CREATE INDEX idx_specimen_person_id ON public.specimen USING btree (person_id);


-- public.survey_conduct definition

-- Drop table

-- DROP TABLE public.survey_conduct;

CREATE TABLE public.survey_conduct (
	survey_conduct_id int8 NOT NULL,
	person_id int8 NOT NULL,
	survey_concept_id int4 NOT NULL,
	survey_start_date date NULL,
	survey_start_datetime timestamp NULL,
	survey_end_date date NULL,
	survey_end_datetime timestamp NOT NULL,
	provider_id int8 NULL,
	assisted_concept_id int4 NOT NULL,
	respondent_type_concept_id int4 NOT NULL,
	timing_concept_id int4 NOT NULL,
	collection_method_concept_id int4 NOT NULL,
	assisted_source_value varchar(50) NULL,
	respondent_type_source_value varchar(100) NULL,
	timing_source_value varchar(100) NULL,
	collection_method_source_value varchar(100) NULL,
	survey_source_value varchar(100) NULL,
	survey_source_concept_id int4 NOT NULL,
	survey_source_identifier varchar(100) NULL,
	validated_survey_concept_id int4 NOT NULL,
	validated_survey_source_value varchar(100) NULL,
	survey_version_number varchar(20) NULL,
	visit_occurrence_id int8 NULL,
	visit_detail_id int8 NULL,
	response_visit_occurrence_id int8 NULL,
	CONSTRAINT xpk_survey PRIMARY KEY (survey_conduct_id)
);
CREATE INDEX idx_survey_person_id ON public.survey_conduct USING btree (person_id);


-- public.trans_coordinates definition

-- Drop table

-- DROP TABLE public.trans_coordinates;

CREATE TABLE public.trans_coordinates (
	trans_coordinates_id int4 NOT NULL,
	trans_seq_annot_id int4 NOT NULL,
	umap_1 float8 NOT NULL,
	umap_2 float8 NOT NULL,
	CONSTRAINT "pk_Coordinates" PRIMARY KEY (trans_coordinates_id)
);


-- public.trans_exp definition

-- Drop table

-- DROP TABLE public.trans_exp;

CREATE TABLE public.trans_exp (
	trans_exp_id int4 NOT NULL,
	device varchar NULL,
	omic_experiment_id int8 NULL,
	trans_group_id int4 NULL,
	CONSTRAINT "pk_Single_cell_experiment" PRIMARY KEY (trans_exp_id)
);


-- public.trans_raw_counts definition

-- Drop table

-- DROP TABLE public.trans_raw_counts;

CREATE TABLE public.trans_raw_counts (
	trans_raw_counts_id int4 NOT NULL,
	sample_cell_id int4 NOT NULL,
	gene_id int4 NOT NULL,
	count_result float8 NOT NULL,
	omic_experiment_id int4 NULL,
	CONSTRAINT "pk_Raw_counts" PRIMARY KEY (trans_raw_counts_id),
	CONSTRAINT trans_raw_counts_un UNIQUE (sample_cell_id)
);


-- public.trans_result definition

-- Drop table

-- DROP TABLE public.trans_result;

CREATE TABLE public.trans_result (
	trans_result_id int4 NOT NULL,
	gene_id int4 NOT NULL,
	p_value int4 NOT NULL,
	p_value_adj int4 NOT NULL,
	avg_log_fc int4 NOT NULL,
	condition_1 varchar NULL,
	condition_2 varchar NULL,
	single_cell_exp_id int4 NOT NULL,
	omic_experiment_id int4 NULL,
	trans_group_id int4 NULL,
	CONSTRAINT "pk_Single_cell_result" PRIMARY KEY (trans_result_id)
);


-- public.trans_seq_annot definition

-- Drop table

-- DROP TABLE public.trans_seq_annot;

CREATE TABLE public.trans_seq_annot (
	trans_seq_annot_id int4 NOT NULL,
	sequence_char varchar NULL,
	ncount_rna int4 NULL,
	nfeature_rna int4 NULL,
	percent_mito int4 NULL,
	percent_rpl int4 NULL,
	percent_rps int4 NULL,
	cell_type varchar NULL,
	sample_cell_id int4 NULL,
	omic_experiment_id int4 NULL,
	CONSTRAINT "Sequence_annot_pkey" PRIMARY KEY (trans_seq_annot_id)
);


-- public.transform_sample definition

-- Drop table

-- DROP TABLE public.transform_sample;

CREATE TABLE public.transform_sample (
	transform_sample_id int4 NOT NULL,
	sample_id int4 NOT NULL,
	lims_id int4 NULL,
	CONSTRAINT transform_sample_pk PRIMARY KEY (transform_sample_id)
);


-- public.visit_detail definition

-- Drop table

-- DROP TABLE public.visit_detail;

CREATE TABLE public.visit_detail (
	visit_detail_id int8 NOT NULL,
	person_id int8 NOT NULL,
	visit_detail_concept_id int4 NOT NULL,
	visit_detail_start_date date NULL,
	visit_detail_start_datetime timestamp NOT NULL,
	visit_detail_end_date date NULL,
	visit_detail_end_datetime timestamp NOT NULL,
	visit_detail_type_concept_id int4 NOT NULL,
	provider_id int8 NULL,
	care_site_id int8 NULL,
	discharge_to_concept_id int4 NOT NULL,
	admitted_from_concept_id int4 NOT NULL,
	admitted_from_source_value varchar(50) NULL,
	visit_detail_source_value varchar(50) NULL,
	visit_detail_source_concept_id int4 NOT NULL,
	discharge_to_source_value varchar(50) NULL,
	preceding_visit_detail_id int8 NULL,
	visit_detail_parent_id int8 NULL,
	visit_occurrence_id int8 NOT NULL,
	CONSTRAINT xpk_visit_detail PRIMARY KEY (visit_detail_id)
);
CREATE INDEX idx_visit_detail_concept_id ON public.visit_detail USING btree (visit_detail_concept_id);
CREATE INDEX idx_visit_detail_person_id ON public.visit_detail USING btree (person_id);


-- public.visit_occurrence definition

-- Drop table

-- DROP TABLE public.visit_occurrence;

CREATE TABLE public.visit_occurrence (
	visit_occurrence_id int8 NOT NULL,
	person_id int8 NOT NULL,
	visit_concept_id int4 NOT NULL,
	visit_start_date date NULL,
	visit_start_datetime timestamp NOT NULL,
	visit_end_date date NULL,
	visit_end_datetime timestamp NOT NULL,
	visit_type_concept_id int4 NOT NULL,
	provider_id int8 NULL,
	care_site_id int8 NULL,
	visit_source_value varchar(50) NULL,
	visit_source_concept_id int4 NOT NULL,
	admitted_from_concept_id int4 NOT NULL,
	admitted_from_source_value varchar(50) NULL,
	discharge_to_source_value varchar(50) NULL,
	discharge_to_concept_id int4 NOT NULL,
	preceding_visit_occurrence_id int8 NULL,
	CONSTRAINT xpk_visit_occurrence PRIMARY KEY (visit_occurrence_id)
);
CREATE INDEX idx_visit_concept_id ON public.visit_occurrence USING btree (visit_concept_id);
CREATE INDEX idx_visit_person_id ON public.visit_occurrence USING btree (person_id);


-- public.vocabulary definition

-- Drop table

-- DROP TABLE public.vocabulary;

CREATE TABLE public.vocabulary (
	vocabulary_id varchar(20) NOT NULL,
	vocabulary_name varchar(255) NOT NULL,
	vocabulary_reference varchar(255) NOT NULL,
	vocabulary_version varchar(255) NULL,
	vocabulary_concept_id int4 NOT NULL,
	CONSTRAINT xpk_vocabulary PRIMARY KEY (vocabulary_id)
);
CREATE UNIQUE INDEX idx_vocabulary_vocabulary_id ON public.vocabulary USING btree (vocabulary_id);


-- public.bacterial_gene foreign keys

ALTER TABLE public.bacterial_gene ADD CONSTRAINT bacterial_gene_fk FOREIGN KEY (species_id) REFERENCES species(species_id);


-- public.care_site foreign keys

ALTER TABLE public.care_site ADD CONSTRAINT fpk_care_site_location FOREIGN KEY (location_id) REFERENCES location(location_id);
ALTER TABLE public.care_site ADD CONSTRAINT fpk_care_site_place FOREIGN KEY (place_of_service_concept_id) REFERENCES concept(concept_id);


-- public.concept foreign keys

ALTER TABLE public.concept ADD CONSTRAINT fpk_concept_class FOREIGN KEY (concept_class_id) REFERENCES concept_class(concept_class_id);
ALTER TABLE public.concept ADD CONSTRAINT fpk_concept_domain FOREIGN KEY (domain_id) REFERENCES domain(domain_id);
ALTER TABLE public.concept ADD CONSTRAINT fpk_concept_vocabulary FOREIGN KEY (vocabulary_id) REFERENCES vocabulary(vocabulary_id);


-- public.concept_class foreign keys

ALTER TABLE public.concept_class ADD CONSTRAINT fpk_concept_class_concept FOREIGN KEY (concept_class_concept_id) REFERENCES concept(concept_id);


-- public.condition_occurrence foreign keys

ALTER TABLE public.condition_occurrence ADD CONSTRAINT fpk_condition_concept FOREIGN KEY (condition_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.condition_occurrence ADD CONSTRAINT fpk_condition_concept_s FOREIGN KEY (condition_source_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.condition_occurrence ADD CONSTRAINT fpk_condition_person FOREIGN KEY (person_id) REFERENCES person(person_id);
ALTER TABLE public.condition_occurrence ADD CONSTRAINT fpk_condition_status_concept FOREIGN KEY (condition_status_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.condition_occurrence ADD CONSTRAINT fpk_condition_type_concept FOREIGN KEY (condition_type_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.condition_occurrence ADD CONSTRAINT fpk_condition_v_detail FOREIGN KEY (visit_detail_id) REFERENCES visit_detail(visit_detail_id);
ALTER TABLE public.condition_occurrence ADD CONSTRAINT fpk_condition_visit FOREIGN KEY (visit_occurrence_id) REFERENCES visit_occurrence(visit_occurrence_id);


-- public."domain" foreign keys

ALTER TABLE public."domain" ADD CONSTRAINT fpk_domain_concept FOREIGN KEY (domain_concept_id) REFERENCES concept(concept_id);


-- public.drug_exposure foreign keys

ALTER TABLE public.drug_exposure ADD CONSTRAINT fpk_drug_concept FOREIGN KEY (drug_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.drug_exposure ADD CONSTRAINT fpk_drug_concept_s FOREIGN KEY (drug_source_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.drug_exposure ADD CONSTRAINT fpk_drug_person FOREIGN KEY (person_id) REFERENCES person(person_id);
ALTER TABLE public.drug_exposure ADD CONSTRAINT fpk_drug_route_concept FOREIGN KEY (route_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.drug_exposure ADD CONSTRAINT fpk_drug_type_concept FOREIGN KEY (drug_type_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.drug_exposure ADD CONSTRAINT fpk_drug_v_detail FOREIGN KEY (visit_detail_id) REFERENCES visit_detail(visit_detail_id);
ALTER TABLE public.drug_exposure ADD CONSTRAINT fpk_drug_visit FOREIGN KEY (visit_occurrence_id) REFERENCES visit_occurrence(visit_occurrence_id);


-- public.epigen_exp foreign keys

ALTER TABLE public.epigen_exp ADD CONSTRAINT epigen_exp_fk FOREIGN KEY (omic_experiment_id) REFERENCES omic_experiment(omic_experiment_id);


-- public.epigen_modification foreign keys

ALTER TABLE public.epigen_modification ADD CONSTRAINT epigen_modification_2fk FOREIGN KEY (epigen_modification_id) REFERENCES epigen_modification(epigen_modification_id);
ALTER TABLE public.epigen_modification ADD CONSTRAINT epigen_modification_fkg FOREIGN KEY (gene_id) REFERENCES gene(gene_id);


-- public.epigen_result foreign keys

ALTER TABLE public.epigen_result ADD CONSTRAINT epigen_results_fk FOREIGN KEY (epigen_experiment_id) REFERENCES epigen_exp(epigen_exp_id);
ALTER TABLE public.epigen_result ADD CONSTRAINT epigen_results_fk2 FOREIGN KEY (omic_experiment_id) REFERENCES omic_experiment(omic_experiment_id);
ALTER TABLE public.epigen_result ADD CONSTRAINT epigen_results_fk3 FOREIGN KEY (epigen_modification_id) REFERENCES epigen_modification(epigen_modification_id);


-- public.gene foreign keys

ALTER TABLE public.gene ADD CONSTRAINT gene_fk FOREIGN KEY (concept_id) REFERENCES concept(concept_id);


-- public.lipid foreign keys

ALTER TABLE public.lipid ADD CONSTRAINT lipid_fk FOREIGN KEY (concept_id) REFERENCES concept(concept_id);


-- public.lipid_exp foreign keys

ALTER TABLE public.lipid_exp ADD CONSTRAINT lipid_exp_fk FOREIGN KEY (omic_experiment_id) REFERENCES omic_experiment(omic_experiment_id);


-- public.lipid_result foreign keys

ALTER TABLE public.lipid_result ADD CONSTRAINT mass_spectrometry_result_exp_fk FOREIGN KEY (lipid_exp_id) REFERENCES lipid_exp(lipid_exp_id);
ALTER TABLE public.lipid_result ADD CONSTRAINT mass_spectrometry_result_fk FOREIGN KEY (lipid_id) REFERENCES lipid(lipid_id);


-- public.location_history foreign keys

ALTER TABLE public.location_history ADD CONSTRAINT fpk_location_history FOREIGN KEY (location_id) REFERENCES location(location_id);
ALTER TABLE public.location_history ADD CONSTRAINT fpk_relationship_type FOREIGN KEY (relationship_type_concept_id) REFERENCES concept(concept_id);


-- public.measurement foreign keys

ALTER TABLE public.measurement ADD CONSTRAINT fpk_measurement_concept FOREIGN KEY (measurement_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.measurement ADD CONSTRAINT fpk_measurement_concept_s FOREIGN KEY (measurement_source_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.measurement ADD CONSTRAINT fpk_measurement_operator FOREIGN KEY (operator_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.measurement ADD CONSTRAINT fpk_measurement_person FOREIGN KEY (person_id) REFERENCES person(person_id);
ALTER TABLE public.measurement ADD CONSTRAINT fpk_measurement_type_concept FOREIGN KEY (measurement_type_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.measurement ADD CONSTRAINT fpk_measurement_unit FOREIGN KEY (unit_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.measurement ADD CONSTRAINT fpk_measurement_v_detail FOREIGN KEY (visit_detail_id) REFERENCES visit_detail(visit_detail_id);
ALTER TABLE public.measurement ADD CONSTRAINT fpk_measurement_value FOREIGN KEY (value_as_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.measurement ADD CONSTRAINT fpk_measurement_visit FOREIGN KEY (visit_occurrence_id) REFERENCES visit_occurrence(visit_occurrence_id);


-- public.metabo_exp foreign keys

ALTER TABLE public.metabo_exp ADD CONSTRAINT metabo_exp_fk FOREIGN KEY (omic_experiment_id) REFERENCES omic_experiment(omic_experiment_id);


-- public.metabo_molecule foreign keys

ALTER TABLE public.metabo_molecule ADD CONSTRAINT metabo_molecule_fk FOREIGN KEY (omic_experiment_id) REFERENCES omic_experiment(omic_experiment_id);
ALTER TABLE public.metabo_molecule ADD CONSTRAINT metabolite_exp_fk FOREIGN KEY (metabo_exp_id) REFERENCES metabo_exp(metabo_exp_id);
ALTER TABLE public.metabo_molecule ADD CONSTRAINT metabolite_fk FOREIGN KEY (concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.metabo_molecule ADD CONSTRAINT metabolite_pathway_fk FOREIGN KEY (pathway_id) REFERENCES metabo_pathway(metabo_pathway_id);


-- public.metabo_pathway foreign keys

ALTER TABLE public.metabo_pathway ADD CONSTRAINT pathway_fk FOREIGN KEY (concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.metabo_pathway ADD CONSTRAINT pathway_fk_1 FOREIGN KEY (sub_pathway_concept_id) REFERENCES concept(concept_id);


-- public.metag_exp foreign keys

ALTER TABLE public.metag_exp ADD CONSTRAINT metag_exp_fk FOREIGN KEY (omic_experiment_id) REFERENCES omic_experiment(omic_experiment_id);


-- public.metag_result foreign keys

ALTER TABLE public.metag_result ADD CONSTRAINT metag_result_fk FOREIGN KEY (metag_exp_id) REFERENCES metag_exp(metag_exp_id);
ALTER TABLE public.metag_result ADD CONSTRAINT metag_result_fk2 FOREIGN KEY (omic_experiment_id) REFERENCES omic_experiment(omic_experiment_id);


-- public.note foreign keys

ALTER TABLE public.note ADD CONSTRAINT fpk_language_concept FOREIGN KEY (language_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.note ADD CONSTRAINT fpk_note_class_concept FOREIGN KEY (note_class_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.note ADD CONSTRAINT fpk_note_encoding_concept FOREIGN KEY (encoding_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.note ADD CONSTRAINT fpk_note_person FOREIGN KEY (person_id) REFERENCES person(person_id);
ALTER TABLE public.note ADD CONSTRAINT fpk_note_type_concept FOREIGN KEY (note_type_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.note ADD CONSTRAINT fpk_note_v_detail FOREIGN KEY (visit_detail_id) REFERENCES visit_detail(visit_detail_id);
ALTER TABLE public.note ADD CONSTRAINT fpk_note_visit FOREIGN KEY (visit_occurrence_id) REFERENCES visit_occurrence(visit_occurrence_id);



-- public.observation foreign keys

ALTER TABLE public.observation ADD CONSTRAINT fpk_observation_concept FOREIGN KEY (observation_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.observation ADD CONSTRAINT fpk_observation_concept_s FOREIGN KEY (observation_source_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.observation ADD CONSTRAINT fpk_observation_person FOREIGN KEY (person_id) REFERENCES person(person_id);
ALTER TABLE public.observation ADD CONSTRAINT fpk_observation_qualifier FOREIGN KEY (qualifier_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.observation ADD CONSTRAINT fpk_observation_type_concept FOREIGN KEY (observation_type_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.observation ADD CONSTRAINT fpk_observation_unit FOREIGN KEY (unit_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.observation ADD CONSTRAINT fpk_observation_v_detail FOREIGN KEY (visit_detail_id) REFERENCES visit_detail(visit_detail_id);
ALTER TABLE public.observation ADD CONSTRAINT fpk_observation_value FOREIGN KEY (value_as_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.observation ADD CONSTRAINT fpk_observation_visit FOREIGN KEY (visit_occurrence_id) REFERENCES visit_occurrence(visit_occurrence_id);


-- public.observation_period foreign keys

ALTER TABLE public.observation_period ADD CONSTRAINT fpk_observation_period_concept FOREIGN KEY (period_type_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.observation_period ADD CONSTRAINT fpk_observation_period_person FOREIGN KEY (person_id) REFERENCES person(person_id);


-- public.omic_experiment foreign keys

ALTER TABLE public.omic_experiment ADD CONSTRAINT omic_experiment_fk FOREIGN KEY (transform_sample_id) REFERENCES transform_sample(transform_sample_id);


-- public.person foreign keys

ALTER TABLE public.person ADD CONSTRAINT fpk_person_care_site FOREIGN KEY (care_site_id) REFERENCES care_site(care_site_id);
ALTER TABLE public.person ADD CONSTRAINT fpk_person_ethnicity_concept FOREIGN KEY (ethnicity_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.person ADD CONSTRAINT fpk_person_ethnicity_concept_s FOREIGN KEY (ethnicity_source_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.person ADD CONSTRAINT fpk_person_gender_concept FOREIGN KEY (gender_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.person ADD CONSTRAINT fpk_person_gender_concept_s FOREIGN KEY (gender_source_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.person ADD CONSTRAINT fpk_person_location FOREIGN KEY (location_id) REFERENCES location(location_id);
ALTER TABLE public.person ADD CONSTRAINT fpk_person_race_concept FOREIGN KEY (race_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.person ADD CONSTRAINT fpk_person_race_concept_s FOREIGN KEY (race_source_concept_id) REFERENCES concept(concept_id);


-- public.procedure_occurrence foreign keys

ALTER TABLE public.procedure_occurrence ADD CONSTRAINT fpk_procedure_concept FOREIGN KEY (procedure_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.procedure_occurrence ADD CONSTRAINT fpk_procedure_concept_s FOREIGN KEY (procedure_source_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.procedure_occurrence ADD CONSTRAINT fpk_procedure_modifier FOREIGN KEY (modifier_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.procedure_occurrence ADD CONSTRAINT fpk_procedure_person FOREIGN KEY (person_id) REFERENCES person(person_id);
ALTER TABLE public.procedure_occurrence ADD CONSTRAINT fpk_procedure_type_concept FOREIGN KEY (procedure_type_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.procedure_occurrence ADD CONSTRAINT fpk_procedure_v_detail FOREIGN KEY (visit_detail_id) REFERENCES visit_detail(visit_detail_id);
ALTER TABLE public.procedure_occurrence ADD CONSTRAINT fpk_procedure_visit FOREIGN KEY (visit_occurrence_id) REFERENCES visit_occurrence(visit_occurrence_id);


-- public.protein foreign keys

ALTER TABLE public.protein ADD CONSTRAINT protein_coding_gene_idfk FOREIGN KEY (coding_gene_id) REFERENCES gene(gene_id);
ALTER TABLE public.protein ADD CONSTRAINT protein_exp_fk FOREIGN KEY (protein_exp_id) REFERENCES proteo_exp(proteo_exp_id);
ALTER TABLE public.protein ADD CONSTRAINT protein_fk FOREIGN KEY (concept_id) REFERENCES concept(concept_id);


-- public.proteo_exp foreign keys

ALTER TABLE public.proteo_exp ADD CONSTRAINT proteo_exp_fk FOREIGN KEY (omic_experiment_id) REFERENCES omic_experiment(omic_experiment_id);


-- public.proteo_result foreign keys

ALTER TABLE public.proteo_result ADD CONSTRAINT protein_result_exp_fk FOREIGN KEY (proteomic_exp_id) REFERENCES proteo_exp(proteo_exp_id);
ALTER TABLE public.proteo_result ADD CONSTRAINT protein_result_fk FOREIGN KEY (protein_id) REFERENCES protein(protein_id);
ALTER TABLE public.proteo_result ADD CONSTRAINT proteo_result_fk FOREIGN KEY (omic_experiment_id) REFERENCES omic_experiment(omic_experiment_id);


-- public.relationship foreign keys

ALTER TABLE public.relationship ADD CONSTRAINT fpk_relationship_concept FOREIGN KEY (relationship_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.relationship ADD CONSTRAINT fpk_relationship_reverse FOREIGN KEY (reverse_relationship_id) REFERENCES relationship(relationship_id);


-- public.species foreign keys

ALTER TABLE public.species ADD CONSTRAINT species_fk FOREIGN KEY (metagenomic_exp_id) REFERENCES metag_exp(metag_exp_id);


-- public.specimen foreign keys

ALTER TABLE public.specimen ADD CONSTRAINT fpk_specimen_concept FOREIGN KEY (specimen_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.specimen ADD CONSTRAINT fpk_specimen_person FOREIGN KEY (person_id) REFERENCES person(person_id);
ALTER TABLE public.specimen ADD CONSTRAINT fpk_specimen_site_concept FOREIGN KEY (anatomic_site_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.specimen ADD CONSTRAINT fpk_specimen_status_concept FOREIGN KEY (disease_status_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.specimen ADD CONSTRAINT fpk_specimen_type_concept FOREIGN KEY (specimen_type_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.specimen ADD CONSTRAINT fpk_specimen_unit_concept FOREIGN KEY (unit_concept_id) REFERENCES concept(concept_id);


-- public.survey_conduct foreign keys

ALTER TABLE public.survey_conduct ADD CONSTRAINT fpk_collection_method FOREIGN KEY (collection_method_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.survey_conduct ADD CONSTRAINT fpk_respondent_type FOREIGN KEY (respondent_type_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.survey_conduct ADD CONSTRAINT fpk_response_visit FOREIGN KEY (response_visit_occurrence_id) REFERENCES visit_occurrence(visit_occurrence_id);
ALTER TABLE public.survey_conduct ADD CONSTRAINT fpk_survey_assist FOREIGN KEY (assisted_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.survey_conduct ADD CONSTRAINT fpk_survey_concept FOREIGN KEY (survey_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.survey_conduct ADD CONSTRAINT fpk_survey_person FOREIGN KEY (person_id) REFERENCES person(person_id);
ALTER TABLE public.survey_conduct ADD CONSTRAINT fpk_survey_source FOREIGN KEY (survey_source_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.survey_conduct ADD CONSTRAINT fpk_survey_timing FOREIGN KEY (timing_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.survey_conduct ADD CONSTRAINT fpk_survey_v_detail FOREIGN KEY (visit_detail_id) REFERENCES visit_detail(visit_detail_id);
ALTER TABLE public.survey_conduct ADD CONSTRAINT fpk_survey_visit FOREIGN KEY (visit_occurrence_id) REFERENCES visit_occurrence(visit_occurrence_id);
ALTER TABLE public.survey_conduct ADD CONSTRAINT fpk_validation FOREIGN KEY (validated_survey_concept_id) REFERENCES concept(concept_id);


-- public.trans_coordinates foreign keys

ALTER TABLE public.trans_coordinates ADD CONSTRAINT "fk_Coordinates_sequence_annot_id" FOREIGN KEY (trans_seq_annot_id) REFERENCES trans_seq_annot(trans_seq_annot_id);


-- public.trans_exp foreign keys

ALTER TABLE public.trans_exp ADD CONSTRAINT single_cell_experiment_fk FOREIGN KEY (omic_experiment_id) REFERENCES omic_experiment(omic_experiment_id);


-- public.trans_raw_counts foreign keys

ALTER TABLE public.trans_raw_counts ADD CONSTRAINT raw_counts_fk_1 FOREIGN KEY (gene_id) REFERENCES gene(gene_id);
ALTER TABLE public.trans_raw_counts ADD CONSTRAINT trans_raw_counts_fk FOREIGN KEY (omic_experiment_id) REFERENCES omic_experiment(omic_experiment_id);


-- public.trans_result foreign keys

ALTER TABLE public.trans_result ADD CONSTRAINT "fk_Single_cell_result_gene_id" FOREIGN KEY (gene_id) REFERENCES gene(gene_id);
ALTER TABLE public.trans_result ADD CONSTRAINT "fk_Single_cell_result_single_cell_exp_id" FOREIGN KEY (single_cell_exp_id) REFERENCES trans_exp(trans_exp_id);
ALTER TABLE public.trans_result ADD CONSTRAINT trans_result_fk FOREIGN KEY (omic_experiment_id) REFERENCES omic_experiment(omic_experiment_id);


-- public.trans_seq_annot foreign keys

ALTER TABLE public.trans_seq_annot ADD CONSTRAINT trans_seq_annot_fk FOREIGN KEY (omic_experiment_id) REFERENCES omic_experiment(omic_experiment_id);


-- public.transform_sample foreign keys

ALTER TABLE public.transform_sample ADD CONSTRAINT transform_sample_fk FOREIGN KEY (sample_id) REFERENCES specimen(specimen_id);


-- public.visit_detail foreign keys

ALTER TABLE public.visit_detail ADD CONSTRAINT fpd_v_detail_visit FOREIGN KEY (visit_occurrence_id) REFERENCES visit_occurrence(visit_occurrence_id);
ALTER TABLE public.visit_detail ADD CONSTRAINT fpk_v_detail_care_site FOREIGN KEY (care_site_id) REFERENCES care_site(care_site_id);
ALTER TABLE public.visit_detail ADD CONSTRAINT fpk_v_detail_concept FOREIGN KEY (visit_detail_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.visit_detail ADD CONSTRAINT fpk_v_detail_concept_s FOREIGN KEY (visit_detail_source_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.visit_detail ADD CONSTRAINT fpk_v_detail_discharge FOREIGN KEY (discharge_to_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.visit_detail ADD CONSTRAINT fpk_v_detail_parent FOREIGN KEY (visit_detail_parent_id) REFERENCES visit_detail(visit_detail_id);
ALTER TABLE public.visit_detail ADD CONSTRAINT fpk_v_detail_person FOREIGN KEY (person_id) REFERENCES person(person_id);
ALTER TABLE public.visit_detail ADD CONSTRAINT fpk_v_detail_preceding FOREIGN KEY (preceding_visit_detail_id) REFERENCES visit_detail(visit_detail_id);
ALTER TABLE public.visit_detail ADD CONSTRAINT fpk_v_detail_type_concept FOREIGN KEY (visit_detail_type_concept_id) REFERENCES concept(concept_id);


-- public.visit_occurrence foreign keys

ALTER TABLE public.visit_occurrence ADD CONSTRAINT fpk_visit_care_site FOREIGN KEY (care_site_id) REFERENCES care_site(care_site_id);
ALTER TABLE public.visit_occurrence ADD CONSTRAINT fpk_visit_concept FOREIGN KEY (visit_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.visit_occurrence ADD CONSTRAINT fpk_visit_concept_s FOREIGN KEY (visit_source_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.visit_occurrence ADD CONSTRAINT fpk_visit_discharge FOREIGN KEY (discharge_to_concept_id) REFERENCES concept(concept_id);
ALTER TABLE public.visit_occurrence ADD CONSTRAINT fpk_visit_person FOREIGN KEY (person_id) REFERENCES person(person_id);
ALTER TABLE public.visit_occurrence ADD CONSTRAINT fpk_visit_preceding FOREIGN KEY (preceding_visit_occurrence_id) REFERENCES visit_occurrence(visit_occurrence_id);
ALTER TABLE public.visit_occurrence ADD CONSTRAINT fpk_visit_type_concept FOREIGN KEY (visit_type_concept_id) REFERENCES concept(concept_id);


-- public.vocabulary foreign keys

ALTER TABLE public.vocabulary ADD CONSTRAINT fpk_vocabulary_concept FOREIGN KEY (vocabulary_concept_id) REFERENCES concept(concept_id);



